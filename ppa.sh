#!/bin/bash
set -e
source /bd_build/buildconfig
set -x

# ppa
add-apt-repository ppa:chris-lea/redis-server -y
add-apt-repository ppa:mc3man/mpv-tests -y
add-apt-repository ppa:git-core/ppa -y
add-apt-repository ppa:numix/ppa -y
add-apt-repository ppa:numix/numix-daily -y
add-apt-repository ppa:webupd8team/java -y
add-apt-repository ppa:webupd8team/terminix -y
add-apt-repository ppa:clipgrab-team/ppa -y
add-apt-repository ppa:certbot/certbot -y
add-apt-repository ppa:uget-team/ppa -y
add-apt-repository ppa:neovim-ppa/stable -y
add-apt-repository ppa:transmissionbt/ppa -y
add-apt-repository ppa:noobslab/macbuntu -y
add-apt-repository ppa:qbittorrent-team/qbittorrent-unstable -y
add-apt-repository ppa:apt-fast/stable -y

curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

curl -sL https://dl.winehq.org/wine-builds/Release.key | apt-key add -
apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

echo "deb [trusted=yes] https://deb.torproject.org/torproject.org bionic main" | tee /etc/apt/sources.list.d/tor.list
echo "deb-src [trusted=yes] https://deb.torproject.org/torproject.org bionic main" | tee -a /etc/apt/sources.list.d/tor.list
