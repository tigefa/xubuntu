FROM tigefa/bionic
MAINTAINER Sugeng Tigefa <tigefa@gmail.com>

COPY . /bd_build

RUN /bd_build/prepare.sh && \
  /bd_build/ppa.sh && \
	/bd_build/system_services.sh && \
	/bd_build/utilities.sh && \
  /bd_build/xubuntu.sh && \
	/bd_build/cleanup.sh

ENV DEBIAN_FRONTEND="teletype" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

####user section####
ENV USER developer
ENV HOME "/home/$USER"

RUN echo 'developer ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo '%developer ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo 'sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo 'www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo '%www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

RUN useradd --create-home --home-dir $HOME --shell /bin/bash $USER && \
  mkdir $HOME/.vnc/

RUN usermod -aG sudo $USER && \
    usermod -aG root $USER && \
    usermod -aG adm $USER && \
    usermod -aG www-data $USER

COPY vnc.sh $HOME/.vnc/
COPY xstartup $HOME/.vnc/
RUN chmod 760 $HOME/.vnc/vnc.sh $HOME/.vnc/xstartup && \
  chown -R $USER:$USER $HOME

USER "$USER"

####Setup a VNC password####
RUN echo vncpassw | vncpasswd -f > ~/.vnc/passwd && \
  chmod 600 ~/.vnc/passwd

EXPOSE 5901

WORKDIR /home/developer

CMD ["/home/developer/.vnc/vnc.sh"]
