#!/bin/bash
set -e
source /bd_build/buildconfig
set -x

## xubuntu and vnc
apt-get install -y tightvncserver xubuntu-desktop wget curl netcat aria2 nano whois figlet p7zip p7zip-full zip unzip rar unrar
apt-get install -y gnome-system-monitor gnome-usage tilix python-pip python3-pip python-apt python-xlib net-tools telnet bash bash-completion lsb-base lsb-release
apt-get install -y dconf-cli dconf-editor clipit xclip flashplugin-installer caffeine python3-xlib breeze-cursor-theme htop lshw
apt-get install -y seahorse pulseaudio-equalizer
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
apt-get update
apt-get install -y git git-lfs bzr mercurial subversion command-not-found command-not-found-data gnupg gnupg2 tzdata gvfs-bin
apt-get install -y *numix*
apt-get install -y *powerline*
apt-get install -y xfce4-*-plugin

ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh
update-alternatives --set x-terminal-emulator $(which tilix)

ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

sudo -E -H pip install -U powerline-shell

apt-get install -y tor deb.torproject.org-keyring
