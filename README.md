# xubuntu

Xubuntu and VNC on Docker

```bash
docker run -d --restart=always --name=xubuntu -p 5901:5901 tigefa/xubuntu
```

Username: developer
Password: vncpasswd
